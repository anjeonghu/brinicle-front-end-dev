/*
App_Framework - v0.1
release date : 2016-XX-XX

Copyright (C) XXXX Ltd., 2016.
All rights reserved.

This software is covered by the license agreement between
the end user and XXXXX Ltd., and may be
used and copied only in accordance with the terms of the
said agreement.

XXXXX Ltd. assumes no responsibility or
liability for any errors or inaccuracies in this software,
or any consequential, incidental or indirect damage arising
out of the use of the software
*/

/**
 * Description: Injector is an dependency Injection class
 * please, add what you want more in order to advance Injector
 */
var app_container = {};

//repository for provider modules(or dependencies)
app_container.$configPhase = {
	$component: {}, //a particular provider module which developer wants to define
	$injectable: {} //other modules as dependencies for making the particular provider
};

//repository for controller, factory modules(or dependencies)
app_container.$runPhase = {
	$component: {}, //a particular provider or controller module which developer wants to define
	$injectable: {} //other modules as dependencies for making the particular factory or controller
};

 ////////////topological sort for dependency relation/////////////////////
function toposort(nodes, edges) {
	var cursor = nodes.length;
	var sorted = new Array(cursor);
	var visited = {};
	var i = cursor;

	while(i--){
		if (!visited[i]){
			visit(nodes[i], i, []);
		}
	}

	return sorted;

	function visit(node, i, predecessors) {
		//check cyclic dependency from the graph which includes vertices and edges
		if(predecessors.indexOf(node) >= 0) {
			throw new Error('Cyclic Dependency: '+ node);
		}

		if(visited[i]) //visited[i] == true
			return;
		visited[i] = true;

		// outgoing - the specific edges among the total edges which the current node is connected via the edge
		var outgoing = edges.filter(function(edge){
			//check all edges in order to check if the edge includes the target node
			return edge[0] === node;
		});
		if(i = outgoing.length){ // i means the counter which the target node needs to visit
			// predecessors is the node array which was already visited in graph
			var preds = predecessors.concat(node);
			
			do {
				var child = outgoing[--i][1]; //the next node from the current node
				visit(child, nodes.indexOf(child), preds);
			} while (i)
		}

		sorted[--cursor] = node;
	}
}

function uniqueNodes(arr){
	var res = [];
	for (var i = 0, len = arr.length; i < len; i++) {
		var edge = arr[i];

		if(res.indexOf(edge[0]) < 0){
			res.push(edge[0]);
		}
		
		if(res.indexOf(edge[1]) < 0) {
			res.push(edge[1]);
		}
	}
	return res;
}
//////////////////////////////////////////////////////

function makeComponent(obj_const){
	//make component such as controller, factory, provier, service with obj_const
	//divide obj_const with constructor and injectable dependencies
	var obj = {};
	var FN_ARGS = /^[^\(]*\(\s*([^\)]*)\)/m;
    var FN_ARG_SPLIT = /,/;
    var FN_ARG = /^\s*(_?)(\S+?)\1\s*$/;
    var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;

	if(typeof obj_const === "function"){
		obj.$const = obj_const;
		obj.$inject = [];

		var fnText = obj_const.toString().replace(STRIP_COMMENTS, '');
        var argDecl = fnText.match(FN_ARGS);
        argDecl[1].split(FN_ARG_SPLIT).forEach(function(arg){
            arg.replace(FN_ARG, function(all, underscore, name){
                obj.$inject.push(name);
            });
        });

	}else if(obj_const.constructor === Array){
		if(obj_const.length === 1){ //why need this if statement? user can add constructor in array form
			obj.$const = obj_const[0];
			obj.$inject = []; //String Array for dependency name, ex)[$ButtonHandler, $ajax]

		}else{
			obj.$const = obj_const[obj_const.length -1]; //the last parameter
			obj.$inject = obj_const.slice(0, obj_const.length-1); //all parameters[Dependency Names] except the last one
		}
	}

	return obj;
}

function binder(component, view){
	//controller
	bindingController(component, view);
}

function bindingController(comp, view){
	//extract each user interaction element from view(area registered in controller)
	var elems = view.querySelectorAll('[clk]');
	console.log("[chs]show the elems: " + elems)
}

function makePhase(obj_phase){
	
	// execute all the methods of obj_phase such as constructor methods, the $config method of provider objects
	var construct, inject;
	var params = [];
	var initOrder = [];
	// the directions of each edge represent the dependency of each module
	// get module names from component object of init phase or run phase by extracting dependency name from component object
	for(var name in obj_phase.$component){
		var module = obj_phase.$component[name]; //ex) init phase(or obj_phase).$component includes (Object)$ButtonHandler, (Object)$ajax, (Object)$tab
		if(module.$inject.length == 0){
			var arr = [];
			arr.push(name);
			arr.push(undefined);		//dummy inject in order to make DAG(for no incoming vertex)
			initOrder.push(arr);
		}else{
			for(var i=0;i<module.$inject.length;i++){
				var arr = [];
				arr.push(name);			//dummy inject in order to make DAG(for incoming vertex)
				arr.push(module.$inject[i]);
				initOrder.push(arr);
			}
		}
	}
	var moduleTopos = toposort(uniqueNodes(initOrder), initOrder);
	var module_count = moduleTopos.length - 1;

	for(var i=module_count; i>= 0; i--){
		var name = moduleTopos[i];
		if(!obj_phase.$component[name]){
			continue;
		}
		var injectLen = obj_phase.$component[name].$inject.length;
		var params = []; //Dependency Objects including module variables, module methods
		// get dependencies from phase object in order to inject them into modules(ex, controller, provider, factory)
		for(var j=0;j<injectLen;j++){
			var moduleName = obj_phase.$component[name].$inject[j];
			
			if(obj_phase.$injectable[moduleName]){
				params.push(obj_phase.$injectable[moduleName]);
			}else{
				throw new Error('No Module : ' + moduleName);
			}
		}

		//check if module is controller, factory, provider
		if(obj_phase.$component[name].type === 'controller'){
			(obj_phase.$component[name].$const).apply(obj_phase.$component[name], params);
			
			document.querySelector("[ctrl=" + name + "]").addEventListener('', function(){
				//when the page renders its elements dynamically from ajax data, binder code causes timing issue, which make the registeration without appending elements into the DOM
				binder(obj_phase.$component[name], document.querySelector("[ctrl=" + name + "]"));
			});
				
			//bind(phase.$$m[name], document.querySelector("*["+ATTR.CTRL+"="+name+"]"));

		}else if(obj_phase.$component[name].type === 'factory'){
			obj_phase.$injectable[name] = obj_phase.$component[name].$const = (obj_phase.$component[name].$const).apply(obj_phase.$component[name], params);//obj_phase.$component[name] - component context, params - dependencies

		}else if(obj_phase.$component[name].type === 'provider'){
			obj_phase.$injectable[name] = obj_phase.$component[name].$const  =  (obj_phase.$component[name].$const).apply(obj_phase.$component[name], params);
			
			// provider needs $config method in order to initialize Factory named with the Provider name
			if(obj_phase.$component[name].$const.$config){
				app_container.defineFactory(name, obj_phase.$component[name].$const.$config);

			}else{
				throw new Error("Provider has to define '$config' property : " + name);
			}
		}
	}
}


/**
 * Description: defineController is to define controller for each view of application by adding services such as factory, provider, service
 * please, add what you want more in order to advance defineController
 * @name - the name of controller 
 * @constructor - object including dependencies and controller constructor
 */
app_container.defineController = function(name, constructor){
	var type = "controller";

	app_container.$runPhase.$component[name] = makeComponent(constructor);
	app_container.$runPhase.$component[name].type = type;
	return this;
}

/**
 * Description: defineFactory is to define factory which developer can add to any controller
 * please, add what you want more in order to advance defineFactory
 * @name - the name of Factory 
 * @constructor - object including dependencies and factory constructor
 */
app_container.defineFactory = function(name, constructor){
	var type = "factory";

	app_container.$runPhase.$component[name] = makeComponent(constructor);
	app_container.$runPhase.$component[name].type = type;
	app_container.$runPhase.$injectable[name] = app_container.$runPhase.$component[name].$const;

	return this;
}

/**
 * Description: defineProvider is to define provider which developer can add to any controller
 * please, add what you want more in order to advance defineProvider
 * @name - the name of Provider 
 * @constructor - object including dependencies and provider constructor
 */
app_container.defineProvider = function(name, constructor){
	var type = "provider";

	app_container.$configPhase.$component[name] = makeComponent(constructor);
	app_container.$configPhase.$component[name].type = type;
	app_container.$configPhase.$injectable[name] = app_container.$configPhase.$component[name].$const;
	
	return this;
}

/**
 * Description: defineService is to define service which developer can add to any controller
 * please, add what you want more in order to advance defineService
 */
/*app_container.defineService = function(name, constructor){
	var type = "service";
}*/

window.addEventListener("load", function(e){
	makePhase(app_container.$configPhase);
	makePhase(app_container.$runPhase);
});

window.app_container = app_container;